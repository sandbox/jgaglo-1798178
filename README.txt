
-- SUMMARY --

The  "Management site banner" project allows to dynamically manage the banners of a site.

-- DESCRIPTION --

  The goal of this module is to manage dynamically banners  on node an its child. It provides "PI Parent Page" content type
  for parent node and use "Basic page" one for child by attaching 3 fields. Banner image is view by enabling
   " Management site banner: Pages intérieures", "Management site banner" and "View: Management site banner: Pages intérieures - Parent"
   blocks.
   
-- REQUIREMENTS --

views : http://drupal.org/project/views,
image,
node_reference from references module : http://drupal.org/project/references . 


-- USAGE -- 
  After installing " Management site banner " module, "PI Parent Page" content type is created and  "pi_content_type_image_fille",
  "pi_content_type_reference" and "pi_content_type_boolean" fields are attached to "Basic page" one.
  The "PI Parent Page" content type is used to create parent nodes and "Basic page" child nodes.
  Set up the "Basic page" by setting "pi_content_type_image_fille" default image which appears when no image is set,
  edit also "pi_content_type_reference" field and check "PI Parent Page" under <<Content types that can be referenced>>.
  
  "pi_content_type_image_fille" field is used to set child node image
  "pi_content_type_reference" field is used to set reference to parent node
  "pi_content_type_boolean" field is used to display parent banner image or not.

  Create parent node by adding "PI Parent Page" content, set its banner.
  Create child node by adding  "Basic page" content, set its banner or check parent banner specifying the parent in the parent list.
-- CONTACT --

Current maintainers:

* References: James GAGLO (fgm) - http://drupal.org/user/1850524
* E-mail : jgaglo@peopleinput.com
		   freemanpolys@gmail.com

